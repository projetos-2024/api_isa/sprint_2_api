const router = require('express').Router()
const userController = require('../controller/userController')
const dbController = require ('../controller/dbController')
const areaController = require('../controller/areaController')

// Rotas para operações relacionadas a usuários
router.post('/cadastro/', userController.postUser);
router.get('/getuser/:id', userController.getUsers);
router.put('/updateuser/', userController.putUser);
router.delete('/deleteuser/:id', userController.deleteUser);

// Rotas para operações de login
router.post('/login', userController.postLogin);
router.get('/login/:id', userController.getLogin);

//rota para consulta das tabelas
router.get("/name", dbController.getName);
router.get("/descUser", dbController.descUser);
router.get("/descArea", dbController.descArea);
router.get("/descBooking", dbController.descBooking);


// Rotas para operações relacionadas a áreas gourmet
router.post("/areapost/", areaController.postArea);
router.get("/areaget/", areaController.getArea);
router.get("/areagetId/:id", areaController.getAreaById);
router.put("/areaput/:id", areaController.putArea);
router.delete("/areadel/:id", areaController.deleteArea);

module.exports = router
