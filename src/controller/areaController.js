//Criação de um array para armazenamento das areas
let areasGourmet = [];

//Criação de constantes para validações feitas em cada metódo
const authCapacity = (capacity) => typeof capacity === "number" && capacity > 0;
const authAvailability = (availability) =>
  ["disponível", "em manutenção", "reservada"].includes(availability);
const authType = (type) =>
  ["CHURRASCO", "SALAO DE FESTA", "PISCINA"].includes(type);

//Criação da classe AreaController onde estarão todos os métodos HTTP
module.exports = class AreaController {
  //Aqui será feito o cadastro de uma nova área com o try catch.
  static async postArea(req, res) {
    try {
      const { capacity, availability, norms, appliances, type } = req.body;

      if (!capacity || !availability || !type) {
        return res
          .status(400)
          .json({ message: "Preencha todos os campos obrigatórios." });
      }

      if (!authCapacity(capacity)) {
        return res
          .status(400)
          .json({ message: "A capacidade deve ser um número positivo." });
      }

      if (!authAvailability(availability)) {
        return res.status(400).json({
          message:
            'A disponibilidade deve ser "disponível", "em manutenção" ou "reservada".',
        });
      }

      if (!authType(type)) {
        return res.status(400).json({
          message:
            'O tipo deve ser "CHURRASCO", "SALAO DE FESTA" ou "PISCINA".',
        });
      }

// Constante para criar a área caso passe por todas as validações,adicionando-a ao array
      const newArea = {
        ID_area: areasGourmet.length + 1,
        capacity,
        availability,
        norms,
        appliances,
        type,
      };
      areasGourmet.push(newArea);
      return res
        .status(200)
        .json({ message: "Área gourmet cadastrada!", area: newArea });
    } catch (error) {
      return res
        .status(500)
        .json({ message: "Ocorreu um erro ao cadastrar a área gourmet." });
    }
  }

  //Aqui será feito a atualização de uma área com o try catch.
  static async putArea(req, res) {
    try {
      const { capacity, availability, type } = req.body;
      const areaId = req.params.id;
      //Busca a área gourmet pelo ID, verificando se é igual ao ID requerido.
      const area = areasGourmet.find(
        (area) => area.ID_area === parseInt(areaId)
      );

      if (!capacity || !availability || !type || !areaId) {
        return res.status(400).json({
          message: "Preencha todos os campos obrigatórios e o ID da área.",
        });
      }

      if (!authCapacity(capacity)) {
        return res
          .status(400)
          .json({ message: "A capacidade deve ser um número positivo." });
      }

      if (!authAvailability(availability)) {
        return res.status(400).json({
          message:
            'A disponibilidade deve ser "disponível", "em manutenção" ou "reservada".',
        });
      }

      if (!authType(type)) {
        return res.status(400).json({
          message:
            'O tipo deve ser "CHURRASCO", "SALAO DE FESTA" ou "PISCINA".',
        });
      }

     // Encontra o índice da área na lista de áreas gourmet
      const areaIndex = areasGourmet.findIndex(
        (area) => area.ID_area === parseInt(areaId)
      );

      // Verifica se a área foi encontrada
      if (areaIndex !== -1) {
        // Atualiza os dados da área encontrada
        areasGourmet[areaIndex] = {
          ...areasGourmet[areaIndex],
          capacity,
          availability,
          type,
        };
        res.status(200).json({ message: "Área editada com sucesso!" });
      } else {
        res.status(404).json({ message: "Área não encontrada." });
      }
    } catch (error) {
      return res
        .status(500)
        .json({ message: "Ocorreu um erro ao editar a área gourmet." });
    }
  }

    //Aqui será feito a deleção de uma área com o try catch.
  static async deleteArea(req, res) {
    try {
      const areaId = req.params.id;

// Encontra o índice da área na lista de áreas gourmet
      const areaIndex = areasGourmet.findIndex(
        (area) => area.ID_area === parseInt(areaId)
      );
      
      //Modificação do conteudo do array pelo metódo splice
      if (areaIndex !== -1) {
        areasGourmet.splice(areaIndex, 1);
        res.status(200).json({ message: "Área deletada com sucesso." });
      } else {
        res.status(404).json({ message: "Área não encontrada." });
      }
    } catch (error) {
      return res
        .status(500)
        .json({ message: "Ocorreu um erro ao deletar a área gourmet." });
    }
  }

      //Aqui será obtido todas as áreas que serão listadas com o try catch.
  static async getArea(req, res) {
    try {
      if (req.params.id !== "") {
        res
          .status(200)
          .json({ message: "As áreas foram encontrada.", areas: areasGourmet });
      } else {
        res.status(400).json({ message: "As áreas não foram encontrada." });
      }
    } catch (error) {
      return res
        .status(500)
        .json({ message: "Ocorreu um erro ao buscar a área." });
    }
  }
  
        //Aqui será obtido as áreas procuradas pelo ID com o try catch.
  static async getAreaById(req, res) {
    try {
      const areaId = req.params.id;

      if (areaId) {
        const area = areasGourmet.find(
          (area) => area.ID_area === parseInt(areaId)
        );
        if (area) {
          return res.status(200).json({ message: "Área encontrada.", area });
        } else {
          return res.status(404).json({ message: "Área não encontrada." });
        }
      } else {
        // Retorna todas as áreas se nenhum ID for fornecido
        return res
          .status(200)
          .json({ message: "Áreas encontradas.", areas: areasGourmet });
      }
    } catch (error) {
      return res
        .status(500)
        .json({ message: "Ocorreu um erro ao buscar a área." });
    }
  }
};
