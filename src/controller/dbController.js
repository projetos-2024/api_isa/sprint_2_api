const connect = require('../db/connect')

module.exports = class ConsultasTabelas{
    //consultar nomes
    static async getName(req, res) {

        const queryShowNames = "show tables";

        connect.query(queryShowNames, async (err, result, fields) => {
            if (err) {
                console.log("Error occurred: " + err);
            }

            const tableNames = result.map(row => row[fields[0].name]);
            res.json({message: "Esses são os nomes das tabelas:", tableNames});
        });//fechamento da query show names
    }//fechamento getName

    //desc de entidades
    static async descUser(req, res) {

        const queryDescUser = "desc user";

        connect.query(queryDescUser, async (err, result, fields) => {
            if (err) {
                console.log("Error occurred: " + err);
            }

            const descUser = result.map(row => row[fields[0].name]);
            res.json({message: "Essa é a descrição da tabela User:", descUser});
        });//fechamento da query DescUser
    }//fechamento descUser

    static async descArea(req, res) {

        const queryDescArea = "desc area";

        connect.query(queryDescArea, async (err, result, fields) => {
            if (err) {
                console.log("Error occurred: " + err);
            }

            const descArea = result.map(row => row[fields[0].name]);
            res.json({message: "Essa é a descrição da tabela Area:", descArea});
        });//fechamento da query Desc
    }//fechamento descArea

    static async descBooking(req, res) {

        const queryDescBooking = "desc booking";

        connect.query(queryDescBooking, async (err, result, fields) => {
            if (err) {
                console.log("Error occurred: " + err);
            }

            const descBooking = result.map(row => row[fields[0].name]);
            res.json({message: "Essa é a descrição da tabela Area:", descBooking});
        });//fechamento da query Desc
    }//fechamento descArea

}//fim da class 

